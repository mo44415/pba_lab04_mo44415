package pba.lab04.proxy;

import lombok.RequiredArgsConstructor;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import pba.lab04.entity.UserEntity;
import pba.lab04.mapper.UserMapper;
import pba.lab04.model.RequestHeader;
import pba.lab04.model.User;
import pba.lab04.model.UserListResponse;
import pba.lab04.model.UserResponse;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserProxy {

    private final UserMapper mapper;

    public UserResponse mapToUserResponse(UserEntity entity) {
        UserResponse userResponse = new UserResponse();

        userResponse.setUser(mapper.mapToUser(entity));
        userResponse.setResponseHeader(createRequestHeader());
        return userResponse;
    }

    public UserListResponse mapToUserListResponse(List<UserEntity> entities) {
        UserListResponse userListResponse = new UserListResponse();

        userListResponse.setUsersList(mapper.mapToUserList(entities));
        userListResponse.setResponseHeader(createRequestHeader());
        return userListResponse;

    }

    public UserEntity mapToUserEntity(User user) {
        return mapper.mapToUserEntity(user);
    }

    public RequestHeader createRequestHeader() {
        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setRequestId(UUID.randomUUID());
        requestHeader.setSendDate(DateTime.now());
        return requestHeader;
    }
}
