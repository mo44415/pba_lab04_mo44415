package pba.lab04.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import pba.lab04.model.User;

import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "users")
public class UserEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.UUID
    )
    private UUID id = null;
    private String name = null;
    private String surname = null;
    private Integer age = null;
    private String personalId = null;
    private User.CitizenshipEnum citizenship;
    private String email = null;
}
